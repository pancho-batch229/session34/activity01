// DEFAULT Setup of Express
// "require" to load the module

const express = require("express");

// Creation of an app/project
// In layman's terms, app is our server
const app = express();
const port = 5000;

// Setup for allowing the server to handle data from request - MIDDLEWARES

// allows system to handle API
app.use(express.json());
// allows your app to read data from forms
app.use(express.urlencoded({extended:true}));

/*

	[SECTION] - Routes

*/

	// Get MEthod
	app.get("/",(req, res) => {
		res.send("my mind's in budapest");
	});


	// Post MEthod
	app.post("/hello",(req, res) => {
		res.send(`Hey there ${req.body.firstname} ${req.body.lastname}, you're a rockstar!`);
	});
	
/*
	MOCK DATABASE 

	an array that will store user object when the "/signup" route is accessed 

*/
	// this will serve as our mock up

	let users = [];

	// Post MEthod - SIGNUP
	app.post("/signup",(req, res) => {
		console.log(req.body);

		// if username and password isn't empty then the user info wll be pushed and stored on our mock database.
		if(req.body.username !== "" && req.body.password !== "") {
			users.push(req.body);
			// template literal ``
			res.send(`User ${req.body.username} successfully registered!`);
		} else {
			res.send("Please input BOTH username and password");
		}
	});


	// Post MEthod - UPDATE
	app.put("/change-password",(req, res) => {
		// creates a variable to store the message to be sent back to the client.
		let message;

		// creates a for loop that will loop through the elements of "users" array
		for(let i = 0; i < users.length; i++) {
			// if the username provided in the client/postman and the username of the current object is the same
			if(req.body.username == users[i].username) {
				// changes the password of the user found by the loop
				users[i].password = req.body.password;

				message = `User ${req.body.username}'s password has been updated.`;
				break;
			} else {
				// if no user found
				message = "User does not exist.";
			}
		} 

		res.send(message);
		console.log(req.body);
	});


// CODE ALONG ACTIVITY

	app.get("/home",(req, res) => {
		res.send("WELCOME!");
	});

// REGISTERED USEWRS

	app.get("/users",(req, res) => {
		res.send(users);
	});

// DELETE USER
	app.delete("/delete-user", (req, res) => {
		let message;

		if(users.length != 0) {
			for(let i = 0; i < users.length; i++) {
				if(req.body.username == users[i].username) {
					users.splice(users[i], 1);
					message = `User ${req.body.username} has been yeeted to oblivion.`
					break;
				} else {
					message = "User does not exist in this plain.";
				}
			}
		}
		res.send(message);
		console.log(req.body);
	});

app.listen(port, () => console.log(`Server is running at port ${port}.`));

